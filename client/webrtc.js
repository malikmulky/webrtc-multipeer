var localVideo;
var firstPerson = false;
var socketCount = 0;
var socketId;
var localStream;
var connections = [];

var peerConnectionConfig = {
  iceServers: [
    { urls: "stun:35.221.172.90:3479" },
    {
      username: "jrr",
      credential: "madalilang",
      urls: "turn:35.221.172.90:3479"
    }
  ]
};

$(document).ready(function() {
  var urlParams = new URLSearchParams(window.location.search);
  const idCourse = urlParams.get("id");
  const roomName = "Room-" + idCourse;
  $(".title").html(roomName);
  pageReady(roomName);
});
function pageReady(roomName) {
  localVideo = document.getElementById("localVideo");

  var constraints = {
    video: true,
    audio: true
  };

  if (navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then(getUserMediaSuccess)
      .then(function() {
        socket = io.connect("https://2350a75c.ngrok.io", { secure: true });
        socket.on("signal", gotMessageFromServer);

        socket.on("connect", function() {
          socketId = socket.id;
          console.log("MysocketID", socketId);

          socket.emit("join", roomName);

          socket.on("user-left", function(id) {
            var video = document.querySelector('[data-socket="' + id + '"]');
            var parentDiv = video.parentElement;
            video.parentElement.parentElement.removeChild(parentDiv);
          });

          socket.on("user-joined", function(id, count, clients) {
            console.log("clients", clients);

            clients.forEach(function(socketListId) {
              if (!connections[socketListId]) {
                connections[socketListId] = new RTCPeerConnection(
                  peerConnectionConfig
                );
                //Wait for their ice candidate
                connections[socketListId].onicecandidate = function() {
                  if (event.candidate != null) {
                    console.log("SENDING ICE");
                    socket.emit(
                      "signal",
                      socketListId,
                      JSON.stringify({ ice: event.candidate })
                    );
                  }
                };

                //Wait for their video stream
                connections[socketListId].onaddstream = function() {
                  console.log("onaddstream", event);
                  gotRemoteStream(event, socketListId);
                };

                //Add the local video stream
                console.log("addStream", localStream);
                connections[socketListId].addStream(localStream);
              }
            });

            //Create an offer to connect with your local description

            if (count >= 2) {
              connections[id].createOffer().then(function(description) {
                connections[id]
                  .setLocalDescription(description)
                  .then(function() {
                    console.log("offer");
                    socket.emit(
                      "signal",
                      id,
                      JSON.stringify({ sdp: connections[id].localDescription })
                    );
                  })
                  .catch(e => console.log(e));
              });
            }
          });
        });
      });
  } else {
    alert("Your browser does not support getUserMedia API");
  }
}

function getUserMediaSuccess(stream) {
  localStream = stream;
  localVideo.srcObject = stream;
}

function gotRemoteStream(event, id) {
  var videos = document.querySelectorAll("video"),
    video = document.createElement("video"),
    div = document.createElement("div"),
    p = document.createElement("p"),
    node = document.createTextNode("SocketId: " + id);
  div.setAttribute("class", "col m1 box-vide");

  

  video.setAttribute("class", "vide-other");
  video.setAttribute("data-socket", id);
  video.srcObject = event.stream;
  video.autoplay = true;

  p.setAttribute('class', 'socket-id')
  p.appendChild(node);

  div.appendChild(video);
  div.appendChild(p);
  document.querySelector(".videos").appendChild(div);
}

function gotMessageFromServer(fromId, message) {
  //Parse the incoming signal
  console.log("fromId", fromId + "  - " + socketId + " NO MESSAGE");
  var signal = JSON.parse(message);

  //Make sure it's not coming from yourself
  if (fromId != socketId) {
    console.log("fromId", fromId + " - socketId: " + socketId);
    if (signal.sdp) {
      console.log("fromId", fromId + " Message: " + signal.sdp.type);
      connections[fromId]
        .setRemoteDescription(new RTCSessionDescription(signal.sdp))
        .then(function() {
          if (signal.sdp.type == "offer") {
            connections[fromId]
              .createAnswer()
              .then(function(description) {
                connections[fromId]
                  .setLocalDescription(description)
                  .then(function() {
                    socket.emit(
                      "signal",
                      fromId,
                      JSON.stringify({
                        sdp: connections[fromId].localDescription
                      })
                    );
                  })
                  .catch(e => console.log(e));
              })
              .catch(e => console.log(e));
          }
        })
        .catch(e => console.log(e));
    }

    if (signal.ice) {
      console.log("fromId", fromId + " Message: ice" + signal.ice);
      connections[fromId]
        .addIceCandidate(new RTCIceCandidate(signal.ice))
        .catch(e => console.log(e));
    }
  }
}
