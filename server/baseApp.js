var express = require("express");

var app = (module.exports = express.createServer());

var io = require("socket.io")(app);

io.on("connection", function(socket) {
  console.log("---------------START---------------");
  console.log("connection-socket.id", socket.id);
  console.log("connection-io.engine.clientsCount", io.engine.clientsCount);
  console.log(
    "connection-Object.keys(io.sockets.clients().sockets)",
    Object.keys(io.sockets.clients().sockets)
  );
  io.sockets.emit(
    "user-joined",
    socket.id,
    io.engine.clientsCount,
    Object.keys(io.sockets.clients().sockets)
  );

  socket.on("signal", (toId, message) => {
	console.log("signal-toId:", toId + " | Socket-id: "+socket.id);
	// console.log("signal-message", message);
    io.to(toId).emit("signal", socket.id, message);
  });

  socket.on("message", function(data) {
	console.log("message-data", data);
    io.sockets.emit("broadcast-message", socket.id, data);
  });

  socket.on("disconnect", function() {
	console.log("disconnect-socket.id", socket.id);
    io.sockets.emit("user-left", socket.id);
  });
});

app.listen(3000, function() {
  console.log(
    "Express server listening on port %d in %s mode",
    app.address().port,
    app.settings.env
  );
});
