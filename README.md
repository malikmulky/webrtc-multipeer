# ProSpark POC WEBEX Multi-Peer
This app using a traditional multi-peer communication using a single signaling server (on server folder).
User can join by room as we know in ProSpark as Course, user can only s

## 1. Setup The Signaling Server (Socket Serever)
Files that used: app.js
The signaling server can be created with nodejs.  Realtime communication is achieved using socket.io
1. `cd server`
2. `npm install`
3. `node app.js`
You can use some tools to run this into your server, like ngrok, or just put it into your own server

## 2. Setup the Client
Client code can be run locally, or from a server.
Files that used: index.html, webex.html, webrtc.js
1. Just change the signaling server URL into your own, in `line 39`
2. Load the index.html page in the browser
3. Choose the room that you want to join (room id are static ids just for testing)


-Only running in chrome
